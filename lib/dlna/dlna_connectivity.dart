import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

class DLNAConnectivity {
  final connectivity = Connectivity();

  StreamSubscription? _connectivitySubscription;

  void init(Function(bool available) callback) {
    _connectivitySubscription = connectivity.onConnectivityChanged
        .listen((List<ConnectivityResult> result) {
      if (result.first == ConnectivityResult.wifi) {
        callback(true);
      } else {
        callback(false);
      }
    });
  }

  Future<bool> checkConnectivityStatus() async {
    List<ConnectivityResult> connectivityResult = await (connectivity.checkConnectivity());
    return connectivityResult.first == ConnectivityResult.wifi;
  }

  void release() {
    _connectivitySubscription?.cancel();
    _connectivitySubscription = null;
  }
}

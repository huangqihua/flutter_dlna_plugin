import 'dart:async';

import 'didl.dart';
import 'dlna_action_result.dart';
import 'dlna_device.dart';
import 'dlna_interface.dart';
import 'dlna_manager.dart';
import 'soap/position_info.dart';

class DlnaAndroidService extends DlnaService {
  DLNAManager dlnaManager = DLNAManager();
  Timer? searchTimer;

  Function? searchCallback;
  Function? searchStopCallback;

  List<dynamic> devices = [];
  List<DLNADevice> deviceList = [];

  @override
  Future<void> init() async {}

  @override
  void setSearchStopCallback(Function searchStopCallback) {
    this.searchStopCallback = searchStopCallback;
  }

  @override
  void setSearchCallback(Function searchCallback) {
    print('dlna.android setSearchCallback');
    this.searchCallback = searchCallback;
    dlnaManager.setRefresher(DeviceRefresher(
        onDeviceUpdate: (DLNADevice device) {
          print('dlna.onDeviceUpdate >>> ${device.deviceName}');
          searchCallback(this.devices);
        },
        onDeviceAdd: (DLNADevice device) {
          print('dlna.onDeviceAdd >>> ${device.deviceName}');
          if (deviceList.contains(device)) {
            searchCallback(this.devices);
            return;
          }
          Map map = Map();
          map["name"] = device.deviceName;
          map["id"] = device.uuid;
          devices.add(map);
          deviceList.add(device);
          searchCallback(this.devices);
        },
        onDeviceRemove: (DLNADevice device) {
          print('dlna.onDeviceAdd >>> ${device.deviceName}');
          this.devices = devices.where((element) => element["id"] != device.uuid).toList();
          this.deviceList = deviceList.where((element) => element.uuid != device.uuid).toList();
          searchCallback(this.devices);
        },
        onPlayProgress: (PositionInfo positionInfo) {
          print('dlna.onPlayProgress >>> ${positionInfo.elapsedPercent}');
        },
        onSearchError: (String message) {
          print('dlna.onSearchError');
        }));
  }

  //搜索设备
  @override
  Future<void> search() async {
    print('dlna.search begin');
    dlnaManager.forceSearch();
    searchTimer?.cancel();
    searchTimer = Timer(Duration(milliseconds: 5000), () {
      print('dlna.search stop');
      dlnaManager.stopSearch();
      this.searchStopCallback!();
    });
  }

  //设置视频地址
  @override
  Future<void> setVideoUrlAndName(String url, String name) async {
    var videoObject = VideoObject(name, url, "http-get:*:video/*");
    DLNAActionResult<String> result = await dlnaManager.actSetVideoUrl(videoObject);
    print('dlna.setVideoUrlAndName >>> ${result.result}');
  }

  //设置设备
  @override
  Future<void> setDevice(String uuid) async {
    DLNADevice device = deviceList.firstWhere((element) => element.uuid == uuid);
    print('dlna.setDevice >>> $device');
    dlnaManager.setDevice(device);
  }

  //启动和播放
  @override
  Future<void> startAndPlay() async {
    print('dlna.startAndPlay >>> you should use actSetVideoUrl');
  }

  //停止
  @override
  Future<void> stop() async {
    DLNAActionResult<String> result = await dlnaManager.actStop();
    print('dlna.stop >>> ${result.result}');
  }
}

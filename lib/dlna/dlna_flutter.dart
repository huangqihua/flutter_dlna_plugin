import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

import 'dlna_android.dart';
import 'dlna_interface.dart';
import 'dlna_ios.dart';

class DlnaFlutter {
  DlnaService? dlnaService;

  Future<void> init() async{
    if (Platform.isIOS) {
      dlnaService = DlnaIosService();
    } else if (Platform.isAndroid) {
      dlnaService = DlnaAndroidService();
    }

    await dlnaService!.init();
  }

  void setSearchCallback(Function searchCallback) {
    if (null != dlnaService)
      dlnaService!.setSearchCallback(searchCallback);
  }

  void setSearchStopCallback(Function searchStopCallback) {
    if (null != dlnaService)
      dlnaService!.setSearchStopCallback(searchStopCallback);
  }

  //搜索设备
  Future<void> search() async{
    if (null != dlnaService)
      await dlnaService!.search();
  }

  //设置视频地址、视频名称
  Future<void> setVideoUrlAndName(String url,String name) async{
    if (null != dlnaService)
      await dlnaService!.setVideoUrlAndName(url,name);
  }


  //设置设备
  Future<void> setDevice(String uuid) async{
    if (null != dlnaService)
      await dlnaService!.setDevice(uuid);
  }

  //启动和播放
  Future<void> startAndPlay() async{
    if (null != dlnaService)
      await dlnaService!.startAndPlay();
  }

  //停止
  Future<void> stop() async{
    if (null != dlnaService)
      await dlnaService!.stop();
  }
}

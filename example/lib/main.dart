import 'package:flutter/material.dart';
import 'package:flutter_dlna_plugin/dlna.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String? deviceName = '';

  DLNADevice? dlnaDevice;

  List<DLNADevice> dlnaDeviceList = [];

  var dlnaManager = DLNAManager();

  @override
  void initState() {
    ///搜索设备的回调
    dlnaManager.setRefresher(DeviceRefresher(onDeviceAdd: (dlnaDevice) {
      deviceName = dlnaDevice.description?.friendlyName;
      dlnaDeviceList.clear();
      dlnaDeviceList.add(dlnaDevice);
      setState(() {});
      print(
          'hqh dlna onDeviceAdd: ${DateTime.now()}\nadd $dlnaDevice  deviceName:${deviceName}');
    }, onDeviceRemove: (dlnaDevice) {
      print('hqh dlna onDeviceRemove: ${DateTime.now()}\nremove $dlnaDevice');
    }, onDeviceUpdate: (dlnaDevice) {
      print('hqh dlna onDeviceUpdate: ${DateTime.now()}\nupdate $dlnaDevice');
    }, onSearchError: (error) {
      print("hqh dlna onSearchError: ${error.toString()}");
    }, onPlayProgress: (positionInfo) {
      print('hqh error current play progress ${positionInfo.relTime}');
    }));
    ///搜索设备
    dlnaManager.startSearch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
          actions: [
            TextButton(
                onPressed: () {
                  dlnaManager.actStop();
                },
                child: const Text(
                  '退出投屏',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                )),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            dlnaManager.stopSearch();
            dlnaManager.startSearch();
          },
          child: Icon(Icons.refresh),
        ),
        body: ListView.separated(
            itemBuilder: (context, index) => itemWidget(dlnaDeviceList[index]),
            separatorBuilder: (context, index) => Container(
                  height: 0.5,
                  color: Colors.grey,
                ),
            itemCount: dlnaDeviceList.length),
      ),
    );
  }

  Widget itemWidget(DLNADevice device) {
    dlnaDevice = device;
    return InkWell(
      onTap: () {
        ///先设置device
        dlnaManager.setDevice(device);
        // http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8
        ///设置播放源
        var dlndObject = VideoObject(
            'title',
            'http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8',
            VideoObject.VIDEO_MP4);
        ///投屏
        dlnaManager.actSetVideoUrl(dlndObject);
      },
      child: Container(
        height: 40,
        padding: const EdgeInsets.only(left: 14),
        alignment: Alignment.centerLeft,
        child: Text(device.description?.friendlyName ?? ''),
      ),
    );
  }
}

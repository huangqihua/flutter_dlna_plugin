# flutter_dlna_plugin

#### 介绍
一款基于DLNA协议的投屏插件



#### 使用说明

1、import

```
import 'package:dlna/dlna.dart';
```

2、Start search

```
var dlnaManager = DLNAManager();
dlnaManager.setRefresher(DeviceRefresher(onDeviceAdd: (dlnaDevice) {
    print('add ' + dlnaDevice.toString());
}, onDeviceRemove: (dlnaDevice) {
    print('remove ' + dlnaDevice.toString());
}, onDeviceUpdate: (dlnaDevice) {
    print('update ' + dlnaDevice.toString());
}, onSearchError: (error) {
    print(error);
}));
dlnaManager.startSearch();

```


3、stop search 

```
dlnaManager.stopSearch();
```

4、Send the video url to the device

```
var videoObject = VideoObject(title, url, VideoObject.VIDEO_MP4);
await dlnaManager.actSetVideoUrl(videoObject);

```

5、Release server

```
dlnaManager.release();
```


